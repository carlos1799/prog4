from libreria.base import Session,Base,engine
from libreria.autor import Autor
from libreria.libro import Libro

Base.metadata.create_all(engine)

session = Session()

print("Datos del Libro")
isbn = input("Introduzca el ISBN: ")
titulo = input("Introduzca el titulo: ")
categoria = input("Introduzca la categoria: ")
print("Datos del Autor")
nombre = input("Introduzca el nombre: ")
nacionalidad = input("Introduzca la nacionalidad: ")

libro = Libro(titulo,categoria)
autor = Autor(nombre,nacionalidad)

libro.autor=[autor]

session.add(libro)
session.add(autor)

session.commit()
session.close()
