from sqlalchemy import Column,String,Integer
from libreria.base import Base




class Autor(Base):
    __tablename__ = 'autor'
    id = Column(Integer, primary_key=True)
    nombre = Column(String(50))
    nacionalidad = Column(String(50))

    def __init__(self,nombre,nacionalidad):
        self.nombre = nombre
        self.nacionalidad = nacionalidad
