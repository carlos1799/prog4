from libreria.base import Session

from libreria.autor import Autor
from libreria.libro import Libro

session = Session()

libros = session.query(Libro).all()
autores = session.query(Autor).all()
if len(libros) == 0:
    print("No hay registro de libros")
else:
    print("Todas los Libros:")
    for libro in libros:
        for autor in autores:
            print(libro.titulo + " fue escrito por " + autor.nombre)

libros_rick = session.query(Libro)
for libro in libros_rick:
    print("\n Rick Riordan ha escrito" + libro.titulo)