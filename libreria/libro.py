from sqlalchemy import Column,String,Integer,ForeignKey,Table
from sqlalchemy.orm import relationship

from libreria.base import Base

lista = Table(
    'lista', Base.metadata,
    Column('ISBN',Integer, ForeignKey('libro.isbn')),
    Column('id_autor',Integer, ForeignKey('autor.id'))
)


class Libro(Base):
    __tablename__ = 'libro'
    isbn = Column(Integer, primary_key=True)
    titulo = Column(String(50))
    categoria = Column(String(50))
    autor = relationship("Autor", secondary=lista)

    def __init__(self, titulo,categoria):
        self.titulo = titulo
        self.categoria = categoria