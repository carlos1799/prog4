from libreria.base import Session,engine,Base
from libreria.autor import Autor
from libreria.libro import Libro


Base.metadata.create_all(engine)

session = Session()