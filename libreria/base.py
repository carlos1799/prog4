from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

engine = create_engine('mysql+pymysql://cj:carlos17@localhost/libreria')

Session = sessionmaker(bind=engine)

Base = declarative_base()
