from flask import Flask
from flask import render_template, request
app = Flask(__name__)

@app.route('/')
def sobremi():
    return render_template('sobremi.html')


@app.route('/landing', methods=['POST'])
def arrancar():
    if request.method == 'POST':
        nom=request.form['nm']
        ape=request.form['ape']
        ed=request.form['ed']
        return render_template('landing.html',nombre=nom,apellido=ape,edad=ed)


@app.route('/landing/sumar',methods=['POST'])
def suma():
    if request.method == 'POST':
        num1=float(request.form['n1'])
        num2=float(request.form['n2'])
        res=num1+num2
        return render_template('suma.html',resultado=res)


if __name__ == '__main__':
    app.run(debug='true')