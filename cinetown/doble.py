from sqlalchemy import Column, String,Integer,Boolean,ForeignKey
from sqlalchemy.orm import relationship,backref

from cinetown.base import Base

class Doble(Base):
    __tablename__ = 'doble'

    id = Column(Integer, primary_key=True)
    nombre = Column(String(50))
    activo = Column(Boolean)
    id_actor = Column(Integer,ForeignKey('actor.id'))
    actor = relationship("Actor", backref=backref("doble",uselist=False))

    def __init__(self,nombre,activo,actor):
        self.nombre = nombre
        self.activo = activo
        self.actor = actor
