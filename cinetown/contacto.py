from sqlalchemy import Column, String,Integer,Boolean,ForeignKey
from sqlalchemy.orm import relationship,backref

from cinetown.base import Base

class Contacto(Base):
    __tablename__ = 'contacto'

    id = Column(Integer,primary_key=True)
    telefono = Column(String(50))
    direccion = Column(String(50))
    id_actor = Column(Integer, ForeignKey('actor.id'))
    actor = relationship('Actor',backref='contacto')

    def __init__(self,telefono,direccion,actor):
        self.telefono = telefono
        self.direccion = direccion
        self.actor = actor
