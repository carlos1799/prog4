from sqlalchemy import Column,String,Integer,Date,ForeignKey,Table
from sqlalchemy.orm import relationship

from cinetown.base import Base

reparto = Table(
    'reparto', Base.metadata,
    Column('id_pelicula',Integer, ForeignKey('pelicula.id')),
    Column('id_actor',Integer, ForeignKey('actor.id'))
)


class Pelicula(Base):
    __tablename__ = 'pelicula'
    id = Column(Integer, primary_key=True)
    titulo = Column(String(50))
    lanzamiento = Column(Date)
    actor = relationship("Actor", secondary=reparto)

    def __init__(self, titulo,lanzmiento):
        self.titulo = titulo
        self.lanzamiento = lanzmiento