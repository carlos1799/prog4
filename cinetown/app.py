from datetime import date
from cinetown.actor import Actor
from cinetown.base import Session,engine,Base
from cinetown.pelicula import Pelicula
from cinetown.doble import Doble
from cinetown.contacto import Contacto

Base.metadata.create_all(engine)

session = Session()