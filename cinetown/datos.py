from datetime import date

from cinetown.actor import Actor
from cinetown.pelicula import Pelicula
from cinetown.contacto import Contacto
from cinetown.doble import Doble
from cinetown.base import Session,engine,Base

Base.metadata.create_all(engine)

session = Session()

#Peliculas
piratas_del_caribe = Pelicula("Piratas del Caribe", date(2014,5,2))
john_wick_3 = Pelicula("John Wick 3",date(2019,7,7))
iron_man = Pelicula("Iron Man 1",date(2008,3,12))

#Actores
jhonny_depp = Actor("Jhonny Depp", date(1968,10,8))
keanu_reeves =Actor("Keanu Reeves", date(1969,11,11))
robert_downey = Actor("Robert Downe Jr.", date(1969,5,5))

#Reparto
piratas_del_caribe.actor=[jhonny_depp]
john_wick_3.actor=[keanu_reeves]
iron_man.actor=[robert_downey]

#Contacto
contacto_jd = Contacto("123456","Su Casa",jhonny_depp)
contacto_kr = Contacto("456369","Su PH",keanu_reeves)
contacto_rd = Contacto("1354664","Su mansion",robert_downey)

session.add(piratas_del_caribe)
session.add(john_wick_3)
session.add(iron_man)

session.add(contacto_jd)
session.add(contacto_kr)
session.add(contacto_rd)

session.commit()
session.close()