from flask import Flask
from flask import render_template
from flask_bootstrap import Bootstrap

app = Flask(__name__)
Bootstrap(app)

@app.route('/')
def index():
    data = [["El Camino de Reyes", "Brandon Sanderson", "Fantasia","https://imagessl6.casadellibro.com/a/l/t5/46/9788466647946.jpg"],
           ["Una magia más oscura", "Victoria Schwab", "Fantasía/Fantasía histórica/ciencia ficción","https://www.planetadelibros.com/usuaris/libros/fotos/282/m_libros/portada_una-magia-mas-oscura-trilogia-sombras-de-magia-vol1_ve-schwab_201903070913.jpg"],
           ["Illuminae", "Amie Kaufman y Jay Kristoff", "Ciencia ficción","https://imagessl9.casadellibro.com/a/l/t5/59/9788420483559.jpg"],
           ["Neuromante"," William Gibson", "Ciencia ficción", "https://images-na.ssl-images-amazon.com/images/I/51uX7aoi4dL._SX331_BO1,204,203,200_.jpg" ],
           ["El último mago", "Lisa Maxwell"," Fantasía/narrativa-juvenil","http://urano.blob.core.windows.net/share/i_portadas/100000119/100000119b.jpg"],
            ["El héroe perdido", "Rick Riordan", "Fantasía/Ficción","https://imagessl2.casadellibro.com/a/l/t5/92/9788415580492.jpg"],
            ["Amanecer rojo", "Pierce Brown", "Ficción distopica/ciencia ficción","https://imagessl4.casadellibro.com/a/l/t5/84/9788427208384.jpg"],
            ["Fuego y Sangre"," George RR Martin", "Literatura Fantástica","https://images-na.ssl-images-amazon.com/images/I/51Lk8pBpcAL._SX334_BO1,204,203,200_.jpg"],
            ["Las pruebas de Apolo", "Rick Riordan", "Fantasía/aventura","https://images-na.ssl-images-amazon.com/images/I/51aY9bBtNiL.jpg"],
            ["Heartless", "Marissa Meyer", "ciencia ficción/Literatura fantástica","https://images-na.ssl-images-amazon.com/images/I/51mk5pk7WbL._SX318_BO1,204,203,200_.jpg"]
           ]
    return render_template('vista.html',data=data)


if __name__ == '__main__':
    app.run(debug='true')