import sqlite3

db = sqlite3.connect('zhillaz.db')

cursor=db.cursor()
cursor.execute('''
CREATE TABLE IF NOT EXISTS CLIENTE (cedula TEXT PRIMARY KEY,
nombre TEXT)
''')
cursor.execute('''
CREATE TABLE IF NOT EXISTS SILLAS (modelo TEXT PRIMARY KEY,
cantidad NUMERIC, precio REAL)
''')
cursor.execute('''
CREATE TABLE IF NOT EXISTS FACTURA (id TEXT PRIMARY KEY,
total REAL, cantidad NUMERIC, fecha TEXT)
''')
cursor.execute('''
CREATE TABLE IF NOT EXISTS "CLIENTFAC" (
	"cedula"	TEXT,
	"id"	TEXT,
	FOREIGN KEY("cedula") REFERENCES "CLIENTE"("cedula"),
	FOREIGN KEY("id") REFERENCES "FACTURA"("id")
);
''')
cursor.execute('''
CREATE TABLE IF NOT EXISTS "PEDIDOS" (
	"modelo"	TEXT,
	"id_fact"	TEXT,
	"cantidad"  NUMERIC,
	FOREIGN KEY("modelo") REFERENCES "SILLAS",
	FOREIGN KEY("id_fact") REFERENCES "FACTURA"("id")
);
''')
db.commit()
cedula=input("Cedula: ")
nombre=input("Nombre: ")
cursor.execute('''INSERT INTO cliente (cedula, nombre)
VALUES(?,?)
''', (cedula, nombre))
db.commit()

cursor.execute('''SELECT * FROM CLIENTE''')
#for row in cursor:
#    print("Nombre: {0}    Cedula: {1}".format(row[1],row[0]))
#cursor.execute('''SELECT COUNT(*) FROM CLIENTE''')
#conteo = cursor.fetchone()
#print("El numero total de clientes es "+ str(conteo[0]))
db.commit()
db.close()